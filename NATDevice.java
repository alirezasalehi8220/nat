import java.net.InetAddress;

public class NATDevice {
    private int port;
    private InetAddress publicIp;
    public void setPortOfNAT(int port){
        this.port = port;
    }
    public void setPublicIpOfNAT(InetAddress incomingIPAddress){
        this.publicIp = incomingIPAddress;
    }

    public int getPortOfNAT(){
        return port;
    }
    public InetAddress getPublicIpOfNAT(){
        return publicIp;
    }

}
